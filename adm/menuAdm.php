<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="../css/gepyme.css" rel="stylesheet" type="text/css">
</head>
<body class="body">
<div align="center">
	<p class="title"><strong>Administracion</strong></p>
	<table>
		<tr align="center">
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Facturas</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaFac.php">Alta de Facturas</a></li>
						<li class="liint"><a target="content" href="./bmFac.php">Modificacion de Facturas</a></li>
					</ul>
					</td></tr>
				</table>
			</td>
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Proveedores</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaProv.php">Alta de Proveedor</a></li>
						<li class="liint"><a target="content" href="./bmProv.php">Modificacion de Proveedor</a></li>
					</ul>
					</td></tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<div class="volver">
		<a href="./indicFac.php" target="content">Indicador Proveedores</a>
	</div>
</div>
</body>	
</html>
