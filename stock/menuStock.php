<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link href="../css/gepyme.css" rel="stylesheet" type="text/css">
</head>
<body class="body">
<div align="center">
	<p class="title"><strong>Gestion de Stock</strong>
	<table>
		<tr align="center">
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Celulares</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaCel.php">Alta de Celular</a></li>
						<li class="liint"><a target="content" href="./bmCel.php">Asig. desde stock</a></li>
						<li class="liint"><a target="content" href="./reasigCel.php">Reasignacion de Celular</a></li>
					</ul>
					</td></tr>
				</table>
			</td>
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Lineas</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaLin.php">Alta de Lineas</a></li>
						<li class="liint"><a target="content" href="./bmLin.php">Asig. desde stock</a></li>
						<li class="liint"><a target="content" href="./reasigLin.php">Reasignacion de Linea</a></li>
					</ul>
					</td></tr>
				</table>
			</td>
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Modems 3/4G</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaMdm.php">Alta de Modem</a></li>
						<li class="liint"><a target="content" href="./bmMdm.php">Asig. desde stock</a></li>
						<li class="liint"><a target="content" href="./reasigMdm.php">Reasignacion de Modem</a></li>
					</ul>
					</td></tr>
				</table>
			</td>
		</tr>
		<tr align="center">
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Notebooks</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaNot.php">Alta de Notebook</a></li>
						<li class="liint"><a target="content" href="./bmNot.php">Asig. desde stock</a></li>
						<li class="liint"><a target="content" href="./reasigNot.php">Reasig. de Notebook</a></td></li>
					</ul>
					</td></tr>
				</table>
			</td>
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Monitores</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaMon.php">Alta de Monitor</a></li>
						<li class="liint"><a target="content" href="./bmMon.php">Asig. desde stock</a></li>
						<li class="liint"><a target="content" href="./reasigMon.php">Reasignacion de Monitor</a></li>
					</ul>
					</td></tr>
				</table>
			</td>
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Servidores</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaSer.php">Alta de Servidor</a></li>
						<li class="liint"><a target="content" href="./bmSer.php">Asig. desde stock</a></li>
						<li class="liint"><a target="content" href="./reasigSer.php">Reasignacion de Servidor</a></li>
					</ul>
					</td></tr>
				</table>
			</td>
			<td>
				<table class="table">
					<tr><td class="titmod"><strong>Redes</strong>
					<ul class="ulint">
						<li class="liint"><a target="content" href="./altaRed.php">Alta de disp. de Red</a></li>
						<li class="liint"><a target="content" href="./bmRed.php">Asig. desde stock</a></li>
						<li class="liint"><a target="content" href="./reasigRed.php">Reasignacion de disp.</a></li>
					</ul>
					</td></tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<div class="volver">
		<a href="./verBienPorPer.php">Listado de Bienes por Persona</a><br>
		<a href="./verBienEnStock.php">Listado de Bienes en Stock</a>
	</div>
</div>
</body>	
</html>
